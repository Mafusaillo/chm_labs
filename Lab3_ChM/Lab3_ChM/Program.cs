﻿using System.Diagnostics.CodeAnalysis;

namespace Lab3ChM
{
    public static class GaussianMethod
    {
        public static double[,] CreateMatrix(int matrixSize)
        {
            Console.WriteLine();
            var resultMatrix = new double[matrixSize, matrixSize + 1];
            Console.ForegroundColor = ConsoleColor.DarkYellow;
            for (var i = 0; i < matrixSize; i++)
            {
                Console.Write("Enter coefficients >> ");
                var coefficients = Console.ReadLine().Split(" ");
                for (var n = 0; n < coefficients.Length; n++)
                {
                    resultMatrix[i, n] = Convert.ToDouble(coefficients[n]);
                }
            }

            return resultMatrix;
        }

        public static void PrintMatrix(double[,] matrix)
        {
            var iterator = 0;
            while (iterator < matrix.GetLength(0))
            {
                var rowMatrix = GetRow(matrix, iterator);
                foreach (var rowMatrixElem in rowMatrix)
                {
                    Console.Write(String.Format("{0,6}||", Math.Round(rowMatrixElem, 2)));
                }

                Console.WriteLine();
                iterator++;
            }
        }

        public static double[] GetRow(double[,] matrix, int rowIndex)
        {
            return Enumerable.Range(0, matrix.GetLength(1))
                .Select(x => matrix[rowIndex, x])
                .ToArray();
        }

        public static double[,] SwapRows(double[,] matrix, int rowIndex, int rowIndex2)
        {
            var rowAtIndex1 = GetRow(matrix, rowIndex);
            for (var j = 0; j < matrix.GetLength(1); j++)
            {
                matrix[rowIndex, j] = matrix[rowIndex2, j];
                matrix[rowIndex2, j] = rowAtIndex1[j];
            }
            return matrix;
        }

        public static double[] MakeDiagCoefficient(double[,] matrix, int rowIndex)
        {
            if (matrix[rowIndex, rowIndex] is 0)
            {
                matrix = SwapRows(matrix, rowIndex, rowIndex + 1);
            }
            var rowAtIndex = GetRow(matrix, rowIndex);
            for (var rowElementIndex = 0;
                 rowElementIndex < rowAtIndex.Length;
                 rowElementIndex++)
            {
                rowAtIndex[rowElementIndex] /= matrix[rowIndex, rowIndex];
            }

            return rowAtIndex;
        }

        public static void SubstractRowDirect(double[,] matrix, int startCoefficient)
        {
            var changedRow = MakeDiagCoefficient(matrix, startCoefficient);
            matrix = ChangeRow(matrix, changedRow, startCoefficient);
            var starter = startCoefficient + 1;
            for (; starter < matrix.GetLength(0); starter++)
            {
                var rowMultiplayer = matrix[starter, startCoefficient];
                for (var j = 0; j < matrix.GetLength(1); j++)
                {
                    matrix[starter, j] -= changedRow[j] * rowMultiplayer;
                }
            }
        }

        public static void SubstractRowReverse(double[,] matrix, int startCoefficient)
        {
            for (var i = startCoefficient - 1; i >= 0; i--)
            {
                var minusRow = GetRow(matrix, startCoefficient);
                var rowMultiplayer = matrix[i, startCoefficient];
                for (var j = 0; j < matrix.GetLength(1); j++)
                {
                    matrix[i, j] -= minusRow[j] * rowMultiplayer;
                }
            }
        }

        public static double[,] ChangeRow(double[,] matrix, double[] row, int rowIndex)
        {
            for (var i = 0; i < matrix.GetLength(1); i++)
            {
                matrix[rowIndex, i] = row[i];
            }

            return matrix;
        }

        public static double[,] SolveJordanGaussianElemenation(double[,] matrix)
        {
            for (var i = 0; i < matrix.GetLength(0); i++)
            {
                SubstractRowDirect(matrix, i);
            }

            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("\nMatrix after Gaussian Direct Elimination\n");
            PrintMatrix(matrix);
            for (var i = matrix.GetLength(0) - 1; i > 0; i--)
            {
                SubstractRowReverse(matrix, i);
            }

            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("\nMatrix after Gaussian Reverse Elimination\n");
            PrintMatrix(matrix);
            return matrix;
        }

        public static double[] ExtractRoots(double[,] matrix)
        {
            Console.ForegroundColor = ConsoleColor.DarkCyan;
            Console.WriteLine("\nRoots are ");
            var roots = new double[matrix.GetLength(0)];
            for (var i = 0; i < matrix.GetLength(0); i++)
            {
                roots[i] = matrix[i, matrix.GetLength(1) - 1];
                Console.WriteLine(String.Format("x{0, 1} >> {1,5}", i + 1, Math.Round(roots[i], 2)));
            }

            return roots;
        }

        public static double[] ExtractAnswers(double[,] matrix)
        {
            var answers = new double[matrix.GetLength(0)];
            for (var i = 0; i < matrix.GetLength(0); i++)
            {
                answers[i] = matrix[i, matrix.GetLength(1) - 1];
            }

            return answers;
        }

        public static void FindFault(double[,] matrix, double[] roots, double[] answers)
        {
            var obtainedAnswers = new double[matrix.GetLength(0)];
            for (var i = 0; i < matrix.GetLength(0); i++)
            {
                for (var j = 0; j < matrix.GetLength(1) - 1; j++)
                {
                    obtainedAnswers[i] += matrix[i, j] * roots[j];
                }
            }

            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.WriteLine("\nObtained faults");
            var finalFault = new double[answers.Length];
            for (var i = 0; i < obtainedAnswers.Length; i++)
            {
                Console.WriteLine(String.Format("x{0, 1} >> {1,3}%", i + 1,
                    Math.Abs(answers[i] - obtainedAnswers[i]) * 100));
            }
        }
    }


    public class MethodCall
    {
        public static void Main(string[] args)
        {
            Console.ForegroundColor = ConsoleColor.DarkBlue;
            Console.WriteLine("Виконав Проць Данило КН-205 Варiант 21\n");
            Console.Write("Enter the amount of coefficients >> ");
            var matrix = GaussianMethod.CreateMatrix(Convert.ToInt32(Console.ReadLine()));
            var cloneMatrix = (double[,])matrix.Clone();
            var answers = GaussianMethod.ExtractAnswers(matrix);
            var roots = GaussianMethod.ExtractRoots(GaussianMethod.SolveJordanGaussianElemenation(matrix));
            GaussianMethod.FindFault(cloneMatrix, roots, answers);
        }
    }

    /*
     * 2 3 5 6
     * 3 2 4 6
     * 4 5 5 3
     *
     * 7 4 10 -7 -7 2
     * -1 -20 -5 -8 -12 12
     * 9 -1 14 -3 3 -9
     * 1 16 25 -19 2 1
     * 7 23 12 2 -17 -4
     */
}